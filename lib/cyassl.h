#ifndef __CYASSL_H
#define __CYASSL_H

#include "config.h"

#ifdef USE_CYASSL

int Curl_cyassl_init(void);

int Curl_cyassl_cleanup(void);

CURLcode Curl_cyassl_connect(struct connectdata *conn, int sockindex);

void Curl_cyassl_close_all(struct SessionHandle *data);

void Curl_cyassl_close(struct connectdata *conn, int sockindex);

ssize_t Curl_cyassl_send(struct connectdata *conn, int sockindex,
                         const void *mem, size_t len);

ssize_t Curl_cyassl_recv(struct connectdata *conn, int sockindex,
                         char *buf, size_t buffersize, bool *wouldblock);

void Curl_cyassl_session_free(void *ptr);

size_t Curl_cyassl_version(char *buffer, size_t size);

int Curl_cyassl_shutdown(struct connectdata *conn, int sockindex);

/* API setup for CyaSSL */
#define curlssl_init Curl_cyassl_init
#define curlssl_cleanup Curl_cyassl_cleanup
#define curlssl_connect Curl_cyassl_connect
#define curlssl_session_free(x)  Curl_cyassl_session_free(x)
#define curlssl_close_all Curl_cyassl_close_all
#define curlssl_close Curl_cyassl_close
#define curlssl_shutdown(x,y) Curl_cyassl_shutdown(x,y)
#define curlssl_set_engine(x,y) (x=x, y=y, CURLE_FAILED_INIT)
#define curlssl_set_engine_default(x) (x=x, CURLE_FAILED_INIT)
#define curlssl_engines_list(x) (x=x, (struct curl_slist *)NULL)
#define curlssl_send Curl_cyassl_send
#define curlssl_recv Curl_cyassl_recv
#define curlssl_version Curl_cyassl_version
#define curlssl_check_cxn(x) (x=x, -1)
#define curlssl_data_pending(x,y) (x=x, y=y, 0)

#endif /* USE_CYASSL */
#endif /* __CYASSL_H */
